# -*- coding: utf-8 -*-
import os
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from app.components.navbar import get_navbar
from app.components.body import get_body
from app.components.table_page import get_table_component
from app.components.bar import get_bar_graph

from app import create_app

external_stylesheets = [dbc.themes.BOOTSTRAP]

env = os.environ.get('FLASK_ENV', 'development')
app = create_app('config.%sConfig' % env.capitalize())
flask_app = app.server


# Get the navbar component
navbar = get_navbar()
body = get_body()

# Get the content component
content = dbc.Container(id="page-content", fluid=True)
app.layout = dbc.Container([dcc.Location(id="url"), navbar, content],
                           fluid=True,
                           className="dash-container",
                           style={"paddingLeft": 0, "paddingRight": 0, })


@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    print('Enter render_page_content:', pathname)
    if pathname in ["/", "/analysis/main"]:
        return html.P("This is the main page!")
    elif pathname == "/analysis/bar":
        return get_bar_graph()
    elif pathname == "/analysis/table":
        print('invoke get_table_component')
        return get_table_component()
        # return html.P("Oh cool, this is page for table show")
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


if __name__ == "__main__":
    app.run_server(debug=True)
