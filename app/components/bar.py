import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}


def get_bar_graph():
    return html.Div(
        children=[
            html.Br(),
            html.H1(children='Dash Data Visualization',
                    style={"text-align": "center"}),
            dcc.Graph(
                id='graph-a',
                figure={
                    'data': [
                        {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
                        {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
                    ],
                    'layout':{
                        'title': 'Dash Data Visualization',
                        'plot_bgcolor': colors['background'],
                        'paper_bgcolor': colors['background'],
                        'font': {
                            'color': colors['text']
                        }
                    }
                }
            ),
        ])
