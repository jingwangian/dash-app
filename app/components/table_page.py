import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

from app.engine import agricultural
from app.services import survey_service


def generate_table(dataframe, max_rows=10):
    table_header = [
        html.Thead(html.Tr([html.Th(col) for col in dataframe.columns]))
    ]

    rows = [html.Tr([
        html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
    ]) for i in range(min(len(dataframe), max_rows))]

    table_body = [html.Tbody(rows)]
    return dbc.Table(table_header + table_body,
                     bordered=True,
                     responsive=True,
                     striped=True)


def get_table_component():
    print('Enter get_table_component')
    # df = agricultural.get_data()

    available_indicators = ['survey', 'observation', 'student', 'teacher']

    try:
        survey_df = survey_service.get_data()
    except Exception as e:
        print('Got error from survey_service:', str(e))

    return dbc.Container([
        dbc.Row([
            dbc.Col(html.H1('agricultural data',
                            style={"text-align": "center"}),
                    width=8),
            dbc.Col(
                dbc.Select(
                    id='xaxis-column',
                    options=[{'label': i, 'value': i} for i in available_indicators],
                    value='Fertility rate, total (births per woman)'
                ),
                width=4,
                align="center")
        ]
        ),

        # dbc.Row(html.H1('agricultural data',
        #                 style={"text-align": "center"})),
        # html.H1('Survey data',
        #         style={"textAlign": "center"}),
        dbc.Row(generate_table(survey_df)),
    ],
        fluid=True)
