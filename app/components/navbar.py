import dash_bootstrap_components as dbc


def get_navbar():
    return dbc.NavbarSimple(
        children=[
            dbc.NavItem(dbc.NavLink("Main", href="/analysis/main")),
            dbc.NavItem(dbc.NavLink("Bar", href="/analysis/bar")),
            dbc.NavItem(dbc.NavLink("Table", href="/analysis/table")),
        ],
        brand="QBENavbar",
        brand_href="#",
        color="primary",
        dark=True,
        fluid=True,
    )
