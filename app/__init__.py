import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
# from flask_cors import CORS
# import jwt
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

external_stylesheets = [dbc.themes.BOOTSTRAP]

# from config import config

print(f'current dir: {os.getcwd()}')

db = SQLAlchemy()
migrate = Migrate()


def create_app(object_name):
    """
    Arguments:
        object_name: the python path of the config object,
                     e.g. project.config.ProdConfig
    """
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    print(app.config)
    # print(app.assets_url_path)

    flask_app = app.server
    flask_app.config.from_object(object_name)

    db.init_app(flask_app)
    migrate.init_app(flask_app, db)

    return app
