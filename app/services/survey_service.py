import pandas as pd

from app.engine.models import Survey


def get_data():
    surveys = Survey.query.all()
    survey_list = [s.to_json() for s in surveys]

    print(survey_list)

    df = pd.DataFrame.from_records(survey_list)

    print(df)

    return df
