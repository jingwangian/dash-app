
import pandas as pd


def get_data():
    '''Return a dataframe of pandas
    '''
    print('Start to fetch data ...')

    df = pd.read_csv('./data/usa-agricultural-exports-2011.csv')

    return df
